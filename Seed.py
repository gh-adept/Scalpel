#!/usr/bin/env python3
##
##  Seed.py
##   
##  Created on October 27, 2016 by Animesh.
##  hello@animesh.td
##  Copyright (c) 2016 Animesh Ltd
##  All Rights Reserved.
##
##  A Python 3 script to seed a Cloudant database on IBM Bluemix.
##

##  Dependencies
##  This script makes use of a third-party package called 'cloudant'.
##  To install this, open the Terminal and type in the following:
##  pip3 install cloudant

from cloudant.client import Cloudant
import socket, errno, time
import os
import json

documentUploded = 0

# Initialise a Cloudant client to manage database connection
Username = "xxxx-xxxxx-xxxxxx"
Password = "xxxx-xxxxx-xxxxxx"
Uri = "https://xxxx-xxxx.cloudant.com"
client = Cloudant(Username, Password, url = Uri)

# Connect to the server
client.connect()

# Perform tasks
session = client.session()
databases = client.all_dbs()
print('Username: {0}'.format(session['userCtx']['name']))
print('Databases: {0}'.format(client.all_dbs()))

# Check if the database exists, if not create one
if "absolute" not in databases:
    absoluteDatabase = client.create_database("absolute")
    if absoluteDatabase.exists():
        print("Database created successfully.")
else:
    absoluteDatabase = client["absolute"] 

# Separate file names into 100-file chunks
def chunks(fileList, n = 100):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(fileList), n):
        yield fileList[i:i + n]

# Loop through every file in the current working directory
chunkCounter = 0

for root, dirs, files in os.walk("."):
    fileChunks = chunks(files)
    for chunk in fileChunks:
        jsonDataArray = []
        chunkCounter += 1
        fileCounter = 0
        print("Uploading", chunkCounter, "of 10,971 chunks...")
        for file in chunk:
            fileCounter += 1
            print(".........File", fileCounter, "of 100")
            jsonFile = open(file)
            jsonData = json.load(jsonFile)
            jsonDataArray.append(jsonData)
            jsonFile.close()
            del jsonData
        print("Bulk uploading...")
        for dataObj in jsonDataArray:
            absoluteDatabase.create_document(dataObj)
            jsonDataArray.remove(dataObj)
        print("Finished uploading", chunkCounter, "of 10,971 chunks.")
        del jsonDataArray

# Disconnect
client.disconnect()

print("Looks like we're done here, lads.")
