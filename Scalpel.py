#!/usr/bin/env python3
##
##  Scalpel.py
##   
##  Created on October 27, 2016 by Animesh.
##  hello@animesh.ltd
##  Copyright (c) 2016 Animesh Ltd
##  All Rights Reserved.
##
##  A Python 3 script to convert UKCP09 climate change projection 
##  `.csv` files into JSON.
##

##  Dependencies
##  This script makes use of a third-party library called `jsonpickle` to 
##  encode Python objects to valid JSON. To install jsonpickle on your computer,
##  open `Terminal` (or `Command Prompt` on Windows) and type in the following:
##  pip3 install -U jsonpickle

##  Script output
##  This script converts UKCP09 climate change projection `.csv` files into a 
##  API-friendly format called JSON. It does so by reading in all the '.csv' present
##  in the root.

##  

import csv
import json
import jsonpickle
import os

numberOfHeaderLines = 0
counter = 0

class Location:
    def __init__(self, locType = None, name = None):
        self.type = locType
        self.name = name

class Variable:
    def __init__(self, name, unit, changeUnit):
        self.name = name
        self.unit = unit
        self.changeIn = changeUnit  

class CDF:
    def __init__(self, cdf, value):
        self.cdf = cdf
        self.value = value

class Projection:
    def __init__(self):
        self._id                = None
        self.dataset            = None
        self.format             = None
        self.values             = None
        self.changeOnly         = None
        self.emissionsScenario  = None
        self.timePeriod         = None
        self.temporalAveraging  = None
        self.spatialAveraging   = None  
        self.variable           = None
        self.location           = None
        self.projections        = []
        self.author             = "Greenhill Sustainability"
        self.copyright          = "Copyright (c) 2016 Greenhill Sustainability. All Rights Reserved."

    # Dataset source options
    DataSources = {
        "prob_land": "Probabilistic Projections over Land",
        "prob_marine": "Probabilistic Projections over Marine Regions"
    }

    # ValueError type options
    ValueTypes = {
        "True": "Change relative to 1961-1990 baseline period",
        "False": "Absolute"
    }

    # Data format options
    DataFormats = {
        "samp_data": "Sampled Data",
        "cdf": "CDF Data"
    }

    # Emissions Scenarios
    EmissionsScenarios = {
        "a1fi": "High (A1FI)",
        "a1b": "Medium (A1B)",
        "b1": "Low (B1)"
    }

    # Spatial Averaging Options
    SpatialAveragingTypes = {
        "grid_box_25km": "Grid Square"
    }

    # Temporal Averaging Options
    TemporalAveragingTypes = {
        "jan": "January",
        "feb": "February",
        "mar": "March",
        "apr": "April",
        "may": "May",
        "jun": "June",
        "jul": "July",
        "aug": "August",
        "sep": "September",
        "oct": "October",
        "nov": "November",
        "dec": "December",
        "djf": "Winter (December, January, February)",
        "mam": "Spring (March, April, May)",
        "jja": "Summer (June, July, August)",
        "son": "Autumn (September, October, November)",
        "ann": "Annual"
    }

    # Climate variables
    ClimateVariables = {
        "temp_dmean_tmean_abs": "Mean Temperature",
        "temp_dmax_tmean_abs": "Maximum Temperature",
        "temp_dmin_tmean_abs": "Minimum Temperature",
        "totalcloud_dmean_tmean_perc": "Total Cloud Cover",
        "precip_dmean_tmean_perc": "Daily Precipitation Rate",
        "relhum_dmean_tmean_perc": "Relative Humidity",
        "mslp_dmean_tmean_abs": "Sea Level Pressure"
    }

    # Units for measuring absolute values
    AbsoluteUnits = {
        "temp_dmean_tmean_abs": "°C",
        "temp_dmax_tmean_abs": "°C",
        "temp_dmin_tmean_abs": "°C",
        "totalcloud_dmean_tmean_perc": "",
        "precip_dmean_tmean_perc": "mm/day",
        "relhum_dmean_tmean_perc": "%",
        "mslp_dmean_tmean_abs": "hPa"
    }

    # Units for measuring relative change
    ChangeUnits = {
        "temp_dmean_tmean_abs": "°C",
        "temp_dmax_tmean_abs": "°C",
        "temp_dmin_tmean_abs": "°C",
        "totalcloud_dmean_tmean_perc": "%",
        "precip_dmean_tmean_perc": "%",
        "relhum_dmean_tmean_perc": "% (of %)",
        "mslp_dmean_tmean_abs": "hPa"
    }

    def parseHeaders(self, file):
        global numberOfHeaderLines
        fileObject = open(file)
        reader = csv.reader(fileObject)
        for row in reader:
            if reader.line_num == 1:
                numberOfHeaderLines = int(row[1])
            self.findSource(row)
            self.findValueType(row)
            self.findDataFormat(row)
            self.findEmissions(row)
            self.findTimePeriod(row)
            self.findTemporalAveraging(row)
            self.findVariable(row)
            self.findLocation(row)
        fileObject.close()

    def parseSampled(self, file):
        fileObject = open(file)
        reader = csv.reader(fileObject)
        for row in reader:
            if reader.line_num <= numberOfHeaderLines:
                continue
            projectionValue = float(row[2])
            self.projections.append(projectionValue)
        fileObject.close()

    def parseCDF(self, file):
        fileObject = open(file)
        reader = csv.reader(fileObject)
        for row in reader:
            if reader.line_num <= numberOfHeaderLines:
                continue
            cdfLevel = float(row[1])
            projectionValue = float(row[2])
            self.projections.append(CDF(cdfLevel, projectionValue))
        fileObject.close()

    def findSource(self, row):
        '''
        Parses the given row from a CSV file to find the dataset used to produce the projections.
        Returns a list of string in the format ["dataset", "<name of the dataset>"]
        '''
        for column in row:
            if "Dataset = " in column:
                index = column.find("=") + 2
                datasetName = column[index:]
                if datasetName in self.DataSources:
                    self.dataset = self.DataSources.get(datasetName)

    def findValueType(self, row):
        '''
        Parses the given row from a CSV file to determine whether the projections are absolute values
        or change relative to a baseline.
        '''
        for column in row:
            if "ChangeOnly = " in column:
                index = column.find("=") + 2
                relativeChange = column[index:]

                if relativeChange == "True":
                    self.changeOnly = True
                else:
                    self.changeOnly = False

                if relativeChange in self.ValueTypes:
                    self.values = self.ValueTypes.get(relativeChange)

    def findDataFormat(self, row):
        '''
        Parses the given row from a CSV file to determine whether the data is equi-probable sampled data
        or CDF data (or PDF data).
        '''
        for column in row:
            if "ProbabilityDataType = " in column:
                index = column.find("=") + 2
                dataFormat = column[index:]
                if dataFormat in self.DataFormats:
                    self.format = self.DataFormats.get(dataFormat)

    def findEmissions(self, row):
        '''
        Parses the given row from a CSV file to determine the emissions scenario which the dataset refers to.
        '''
        for column in row:
            if "EmissionsScenarios = " in column:
                index = column.find("=") + 2
                emissions = column[index:]
                if emissions in self.EmissionsScenarios:
                    self.emissionsScenario = self.EmissionsScenarios.get(emissions)

    def findTimePeriod(self, row):
        '''
        Parses the given row from a CSV file to determine the time period which the dataset refers to.
        '''
        for column in row:
            if "TimeSlices = " in column:
                index = column.find("=") + 2
                period = column[index:]
                self.timePeriod = period

    def findTemporalAveraging(self, row):
        '''
        Parses the given row from a CSV file to determine the temporal averaging used to prepare the dataset.
        '''
        for column in row:
            # Temporal Averaging
            if "MeaningPeriod = " in column:
                index = column.find("=") + 2
                averagingPeriod = column[index:]
                if averagingPeriod in self.TemporalAveragingTypes:
                    self.temporalAveraging = self.TemporalAveragingTypes.get(averagingPeriod)
            # Spatial Averaging
            elif "LocationType = " in column:
                index = column.find("=") + 2
                locType = column[index:]
                if locType in self.SpatialAveragingTypes:
                    self.spatialAveraging = self.SpatialAveragingTypes.get(locType)

    def findVariable(self, row):
        '''
        Parses the given row from a CSV file to determine the climate variable the dataset refers to.
        '''
        for column in row:
            if "Variables = " in column:
                index = column.find("=") + 2
                variable = column[index:]
                if variable in self.ClimateVariables:
                    name = self.ClimateVariables.get(variable)
                    unit = self.AbsoluteUnits.get(variable)
                    changeReportedIn = self.ChangeUnits.get(variable)
                    self.variable = Variable(name, unit, changeReportedIn)
    
    def findLocation(self, row):
        '''
        Parses the given row from a CSV file to determine the location the dataset refers to.
        '''
        for column in row:
            if "Location = " in column:
                index = column.find("=") + 2
                loc = column[index:]
                if self.location is None:
                    self.location = Location(name = loc)
                    self.location.type = self.spatialAveraging
                else:
                    self.location.name = loc
                    self.location.type = self.spatialAveraging


# Loop through every file in the current working directory
for root, dirs, files in os.walk("."):
    for file in files:
        # Skip non-CSV files
        if not file.endswith(".csv"):
            continue
        
        # Remove the headers
        projection = Projection()
        projection.parseHeaders(os.path.join(root, file))
        if projection.format == "Sampled Data":
            projection.parseSampled(os.path.join(root, file))
        elif projection.format == "CDF Data":
            projection.parseCDF(os.path.join(root, file))

        # Write out the CSV file 
        # fileObject = open(os.path.join("Edited", file), "w", newline = "")
        # writer = csv.writer(fileObject)
        # for row in projection.describe():
        #   writer.writerow(row)
        # fileObject.close()     

        # Set _id in JSON for database indexing
        ind = file.find(".csv")
        projection._id = file[6:ind].lower().replace("_", "-")

        # Write out the JSON file
        counter += 1
        index = file.find(".csv")
        jsonFileName = file[0:index] + ".json"
        print(counter, ": Writing", jsonFileName, "...")
        if projection.format == "Sampled Data":
            # Create a directory to save results. No error is raised if this 
            # directory already exists.
            os.makedirs("Sampled-JSON", exist_ok = True)
            jsonFile = open(os.path.join("Sampled-JSON", jsonFileName), "w")
        elif projection.format == "CDF Data":
            os.makedirs("CDF-JSON", exist_ok = True)
            jsonFile = open(os.path.join("CDF-JSON", jsonFileName), "w")
        jsonData = jsonpickle.encode(projection, False)
        jsonFile.write(jsonData)
        jsonFile.close()